using NUnit.Framework;
using Skelp.DataScience.Entropy;
using System.Linq;

namespace Skelp.DataScience.Tests.Entropy
{
    public class ArbitraryEntropyAnalyzerTests
    {
        [Test]
        public void CalculateChunkEntropies_eightBit_EvenChunks_ReturnsExpected()
        {
            var source = new byte[512];

            // Equal probabilities
            for (int i = 0; i < 256; i++)
            {
                source[i] = (byte)i;
            }

            // One-sided probabilities
            for (int i = 256; i < 384; i++)
            {
                source[i] = 1;
            }
            var _analyzer = new ArbitraryEntropyAnalyzer(source, 8);
            var results = _analyzer.GetChunkEntropies(256).ToArray();

            // Equal probabilities
            Assert.That(results[0], Is.EqualTo(8.0m));

            // One-sided probabilities
            Assert.That(results[1], Is.EqualTo(1.0m));
        }

        [Test]
        public void CalculateEntropy_eightBit_EqualProbabilities_ReturnsEight()
        {
            var source = new byte[256];
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = (byte)i;
            }
            var _analyzer = new ArbitraryEntropyAnalyzer(source, 8);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.EqualTo(8.0m));
        }

        [Test]
        public void CalculateEntropy_eightBit_OneSidedProbabilities_ReturnsOne()
        {
            var source = new byte[256];
            for (int i = 0; i < 128; i++)
            {
                source[i] = 1;
            }
            var _analyzer = new ArbitraryEntropyAnalyzer(source, 8);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.EqualTo(1.0m));
        }

        [Test]
        public void CalculateEntropy_nineBit_EqualProbabilities_ReturnsEight()
        {
            var source = new byte[576];
            for (int i = 0; i < source.Length / 2; i++)
            {
                source[i] = byte.MaxValue;
            }

            var _analyzer = new ArbitraryEntropyAnalyzer(source, 9);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.GreaterThanOrEqualTo(0.995).And.LessThanOrEqualTo(1.005));
        }

        [Test]
        public void CalculateEntropy_threeBit_EqualProbabilities_ReturnsEight()
        {
            var source = new byte[3]
            {
                    0b0000_0101,
                    0b0011_1001,
                    0b0111_0111
            };

            var _analyzer = new ArbitraryEntropyAnalyzer(source, 3);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.EqualTo(3.0m));
        }
    }
}
