using NUnit.Framework;
using Skelp.DataScience.Entropy;
using System.Linq;

namespace Skelp.DataScience.Tests.Entropy
{
    public class ByteEntropyAnalyzerTests
    {
        [Test]
        public void CalculateChunkEntropies_EvenChunks_ReturnsExpected()
        {
            var source = new byte[512];

            // Equal probabilities
            for (int i = 0; i < 256; i++)
            {
                source[i] = (byte)i;
            }

            // One-sided probabilities
            for (int i = 256; i < 384; i++)
            {
                source[i] = 1;
            }
            var _analyzer = new ByteEntropyAnalyzer(source);
            var results = _analyzer.GetChunkEntropies(256).ToArray();

            // Equal probabilities
            Assert.That(results[0], Is.EqualTo(8.0m));

            // One-sided probabilities
            Assert.That(results[1], Is.EqualTo(1.0m));
        }

        [Test]
        public void CalculateEntropy_EqualProbabilities_ReturnsEight()
        {
            var source = new byte[256];
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = (byte)i;
            }
            var _analyzer = new ByteEntropyAnalyzer(source);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.EqualTo(8.0m));
        }

        [Test]
        public void CalculateEntropy_OneSidedProbabilities_ReturnsOne()
        {
            var source = new byte[256];
            for (int i = 0; i < 128; i++)
            {
                source[i] = 1;
            }
            var _analyzer = new ByteEntropyAnalyzer(source);
            var result = _analyzer.GetEntropy();
            Assert.That(result, Is.EqualTo(1.0m));
        }
    }
}
