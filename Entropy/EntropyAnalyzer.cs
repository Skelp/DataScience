﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Skelp.DataScience.Entropy
{
    /// <summary>
    /// Provides methods to analyze data for its entropy.
    /// </summary>
    public abstract class EntropyAnalyzer : IEntropyAnalyzer
    {
        public EntropyAnalyzer(IEnumerable<byte> information, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : this(information.ToArray(), entropyUnit)
        {
            if (information is null)
            {
                throw new ArgumentNullException(nameof(information));
            }
        }

        public EntropyAnalyzer(byte[] information, EntropyUnit entropyUnit = EntropyUnit.Bit)
        {
            if (information is null)
            {
                throw new ArgumentNullException(nameof(information));
            }

            Stream = new MemoryStream(information);
            SetEntropyUnit(entropyUnit);
        }

        public EntropyAnalyzer(Stream stream, EntropyUnit entropyUnit = EntropyUnit.Bit)
        {
            Stream = stream ?? throw new ArgumentNullException(nameof(stream));
            SetEntropyUnit(entropyUnit);
        }

        protected Func<decimal, decimal> EntropyLogFunc { get; set; }

        protected Stream Stream { get; set; }

        public abstract IEnumerable<decimal> GetChunkEntropies(int chunkSize);

        public abstract decimal GetEntropy();

        public void SetEntropyUnit(EntropyUnit entropyUnit)
        {
            switch (entropyUnit)
            {
                case EntropyUnit.Bit:
                    EntropyLogFunc = (x) => (decimal)Math.Log((double)x, 2);
                    break;

                case EntropyUnit.Nat:
                    EntropyLogFunc = (x) => (decimal)Math.Log((double)x);
                    break;

                case EntropyUnit.Dit:
                    EntropyLogFunc = (x) => (decimal)Math.Log10((double)x);
                    break;

                default:
                    throw new NotImplementedException(entropyUnit.ToString());
            }
        }

        protected decimal CalculateEntropy(decimal[] probabilities)
        {
            decimal result = 0;

            for (int i = 0; i < probabilities.Length; i++)
            {
                if (probabilities[i] == 0)
                {
                    continue;
                }

                result += probabilities[i] * EntropyLogFunc(probabilities[i]);
            }

            return decimal.Negate(result);
        }

        protected decimal[] CalculateProbabilities(long startPosition, long endPosition, uint[] occurrences)
        {
            var probabilities = new decimal[occurrences.Length];
            var informationSize = (endPosition + 1) - startPosition;

            for (int i = 0; i < occurrences.Length; i++)
            {
                probabilities[i] = decimal.Divide(occurrences[i], informationSize);
            }

            return probabilities;
        }
    }
}
