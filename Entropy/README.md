
#  Entropy [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) ![Nuget](https://img.shields.io/nuget/v/Skelp.DataScience.Entropy) 

This repository provides two different classes for analyzing the entropy of any given data.
One class is suited for best performance with byte-sized content, the other for arbitrary bit-sized content.

These classes support use with `Stream` objects, IEnumerables of type `byte`, `byte` arrays.
In addition, multiple well-known entropy units are supported as output.

## Table of Contents

1. [Installation](#installation)
2. [Getting Started](#getting-started)

## Installation

Simply use the Visual Studio NuGet Package Manager and look for `DataScience.Entropy`.

Alternatively, you can use the Package Manager Console as such:
> `Install-Package Skelp.DataScience.Entropy`

If you want to use the .NET CLI, you can use the following command:
> `dotnet add package Skelp.DataScience.Entropy`

## Getting Started

### Using Default Parameters

```csharp
using Skelp.DataScience.Entropy;
```
`[...]`
```csharp
// 'source' can be of type Stream, IEnumerable<byte>, or byte[]
byte[] source = yourSource;
var _analyzer = new ByteEntropyAnalyzer(source);
// Using default, EntropyUnit.Bit (Shannon)
var result = _analyzer.GetEntropy();
```

### Using Different EntropyUnit

```csharp
using Skelp.DataScience.Entropy;
```
`[...]`
```csharp
// 'source' can be of type Stream, IEnumerable<byte>, or byte[]
byte[] source = yourSource;

// Bit (shannon)
var _analyzer = new ByteEntropyAnalyzer(source, EntropyUnit.Bit);
// Nat (natural unit of information)
//  _analyzer = new ByteEntropyAnalyzer(source, EntropyUnit.Nat);
// Dit (hartley)
//  _analyzer = new ByteEntropyAnalyzer(source, EntropyUnit.Dit);

var resultInBit = _analyzer.GetEntropy();
_analyzer.SetEntropyUnit(EntropyUnit.Nat);
var resultInNat =_analyzer.GetEntropy();
_analyzer.SetEntropyUnit(EntropyUnit.Dit);
var resultInDit =_analyzer.GetEntropy();
```