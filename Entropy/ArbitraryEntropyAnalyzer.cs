﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Skelp.DataScience.Entropy
{
    /// <summary>
    /// Provides methods to analyze data for its entropy.
    /// <para>
    /// Suited for performing well on data-units in arbitrary bit-sizes. Use <see
    /// cref="ByteEntropyAnalyzer"/> if the data consists of byte-sized content.
    /// </para>
    /// </summary>
    public class ArbitraryEntropyAnalyzer : EntropyAnalyzer
    {
        private int _bits;

        private ulong _possibleValues;

        public ArbitraryEntropyAnalyzer(IEnumerable<byte> information, int bits, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(information, entropyUnit)
        {
            SetupFields(bits);
        }

        public ArbitraryEntropyAnalyzer(byte[] information, int bits, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(information, entropyUnit)
        {
            SetupFields(bits);
        }

        public ArbitraryEntropyAnalyzer(Stream stream, int bits, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(stream, entropyUnit)
        {
            SetupFields(bits);
        }

        public override IEnumerable<decimal> GetChunkEntropies(int chunkSize)
        {
            var chunkSizeInBits = chunkSize * _bits;
            var bytesPerChunk = chunkSizeInBits / 8m;

            var results = new decimal[(long)Math.Floor(Stream.Length / bytesPerChunk)];

            for (int i = 0; i < results.Length; i++)
            {
                results[i] = GetEntropy(i * (long)bytesPerChunk, i * (long)bytesPerChunk + (long)bytesPerChunk - 1);
            }

            return results;
        }

        public override decimal GetEntropy() => GetEntropy(0, (long)ConvertPosition(Stream.Length, _bits, fromArbitraryBits: false) - 1);

        private uint[] CalculateOccurrences(long startPosition, long endPosition)
        {
            var occurrences = new Dictionary<ulong, uint>();
            var byteStartPosition = (long)Math.Round(ConvertPosition(startPosition, _bits, fromArbitraryBits: true));
            Stream.Position = byteStartPosition;
            var byteEndPosition = (long)Math.Floor(ConvertPosition(endPosition, _bits, fromArbitraryBits: true));

            BitArray nextValue = new BitArray(_bits);
            var bitIndex = 0;

            while (Stream.Position <= byteEndPosition)
            {
                int nextByte = Stream.ReadByte();
                if (nextByte == -1)
                {
                    break;
                }

                for (int i = 0; i < 8; i++)
                {
                    nextValue[bitIndex] = GetBit((byte)nextByte, i);

                    bitIndex++;

                    if (bitIndex >= _bits)
                    {
                        var nextValUlong = BitArrayConverter.BitArrayToUlong(nextValue);
                        if (occurrences.ContainsKey(nextValUlong) == false)
                        {
                            occurrences.Add(nextValUlong, 0);
                        }
                        occurrences[nextValUlong]++;
                        bitIndex = 0;
                    }
                }
            }

            return occurrences.Values.ToArray();
        }

        private decimal ConvertPosition(long position, int bits, bool fromArbitraryBits = true)
        {
            return position / (fromArbitraryBits ? 8m : bits) * (fromArbitraryBits ? bits : 8m);
        }

        private bool GetBit(byte x, int bit)
        {
            return (x & (128 >> bit)) != 0;
        }

        private decimal GetEntropy(long startPosition, long endPosition)
        {
            var occurrences = CalculateOccurrences(startPosition, endPosition);
            var probabilities = CalculateProbabilities(startPosition, endPosition, occurrences);
            return CalculateEntropy(probabilities);
        }

        private void SetupFields(int bits)
        {
            _bits = bits;
            _possibleValues = (ulong)Math.Pow(2, bits);
        }
    }
}
