﻿using System;
using System.Collections;

namespace Skelp.DataScience.Entropy
{
    internal class BitArrayConverter
    {
        // Credit for original to William Casarin on stackoverflow https://stackoverflow.com/a/51430897
        public static ulong BitArrayToUlong(BitArray ba)
        {
            var len = Math.Min(64, ba.Length);
            ulong n = 0;
            for (int i = 0; i < len; i++)
            {
                if (ba.Get((len - 1) - i))
                    n |= (ulong)0b0000_0001 << i;
            }
            return n;
        }
    }
}
