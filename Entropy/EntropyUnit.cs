﻿namespace Skelp.DataScience.Entropy
{
    public enum EntropyUnit
    {
        /// <summary>
        /// Shannon.
        /// </summary>
        Bit,

        /// <summary>
        /// Natural unit of information.
        /// </summary>
        Nat,

        /// <summary>
        /// Hartley.
        /// </summary>
        Dit,
    }
}
