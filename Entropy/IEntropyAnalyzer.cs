﻿using System.Collections.Generic;

namespace Skelp.DataScience.Entropy
{
    public interface IEntropyAnalyzer
    {
        IEnumerable<decimal> GetChunkEntropies(int chunkSize);

        /// <summary>
        /// Analyzes data and calculates its entropy in its given <see cref="EntropyUnit"/>.
        /// </summary>
        /// <returns>The entropy of the provided data in <see cref="EntropyUnit"/>.</returns>
        decimal GetEntropy();

        void SetEntropyUnit(EntropyUnit entropyUnit);
    }
}
