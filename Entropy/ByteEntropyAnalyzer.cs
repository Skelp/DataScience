﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Skelp.DataScience.Entropy
{
    /// <summary>
    /// Provides methods to analyze data for its entropy.
    /// <para>Suited for performing well on data-units in sizes of bytes.</para>
    /// </summary>
    public class ByteEntropyAnalyzer : EntropyAnalyzer
    {
        public ByteEntropyAnalyzer(IEnumerable<byte> information, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(information, entropyUnit)
        {
        }

        public ByteEntropyAnalyzer(byte[] information, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(information, entropyUnit)
        {
        }

        public ByteEntropyAnalyzer(Stream stream, EntropyUnit entropyUnit = EntropyUnit.Bit)
            : base(stream, entropyUnit)
        {
        }

        public override IEnumerable<decimal> GetChunkEntropies(int chunkSize)
        {
            var results = new decimal[Stream.Length / chunkSize];

            for (int i = 0; i < results.Length; i++)
            {
                results[i] = GetEntropy(i * chunkSize, (i * chunkSize + chunkSize) - 1);
            }

            return results;
        }

        public override decimal GetEntropy() => GetEntropy(0, Stream.Length - 1);

        private uint[] CalculateOccurrences(long startPosition, long endPosition)
        {
            var occurrences = new Dictionary<ulong, uint>();
            Stream.Position = startPosition;

            int nextByte;

            while (Stream.Position <= endPosition)
            {
                nextByte = Stream.ReadByte();
                if (occurrences.ContainsKey((ulong)nextByte) == false)
                {
                    occurrences.Add((ulong)nextByte, 0);
                }
                occurrences[(ulong)nextByte]++;
            }

            return occurrences.Values.ToArray();
        }

        private decimal GetEntropy(long startPosition, long endPosition)
        {
            var occurrences = CalculateOccurrences(startPosition, endPosition);
            var probabilities = CalculateProbabilities(startPosition, endPosition, occurrences);
            return CalculateEntropy(probabilities);
        }
    }
}
