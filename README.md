
#  DataScience [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

This repository provides individual projects dedicated to helpful data science tools.

## Project Pages

1. [DataScience.Entropy](https://gitlab.com/Skelp/DataScience/-/tree/master/Entropy)
